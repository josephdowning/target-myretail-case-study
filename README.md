# MyRetail API

# Table of Contents
- [MyRetail API](#myretail-api)
- [Table of Contents](#table-of-contents)
- [Tasks](#tasks)
  - [Building](#building)
  - [Running dependencies](#running-dependencies)
  - [Running](#running)
  - [Running acceptance tests](#running-acceptance-tests)
- [API Endpoints](#api-endpoints)
  - [Get Product Info](#get-product-info)
  - [Update Product Price](#update-product-price)
- [Admin Endpoints](#admin-endpoints)
- [Future Improvements](#future-improvements)

# Tasks

## Building

`./gradlew build`

Will build the application and run all unit tests

## Running dependencies

`docker-compose up`

Will run MongoDB as a docker container (if docker-compose is installed)

## Running

`./gradlew bootRun`

Will run the application. This requires MongoDB to be running locally.

## Running acceptance tests

`./gradlew runAcceptance`

Will run acceptance tests against a locally running version of the application.

# API Endpoints

## Get Product Info

Returns product details and price for the specified product

Route: `GET /myretail/v1/products/{productId}`

Example response payload:
 ```json
{
  "id": "13860428",
  "title": "The Big Lebowski (Blu-ray)",
  "current_price": {
    "value": 9.99,
    "currency_code": "USD"
  }
}
```

## Update Product Price

Accepts updated price information for the specified product

Route: `PUT /myretail/v1/products/{productId}`

Example request payload:
```json
{
  "value": 9.99,
  "currency_code": "USD"
}
```

Example response payload:
```json
{
  "id": "13860428",
  "current_price": {
    "value": 9.99,
    "currency_code": "USD"
  }
}
```

# Admin Endpoints

The following is a list of a few of the admin endpoints available

- `GET /actuator` returns a list of all admin endpoints available
- `GET /actuator/health` returns health checks for the application
- `GET /actuator/info` returns info about the running application version
- `GET /actuator/mappings` returns a list of all endpoints exposed by the application
- `GET /actuator/metrics` returns a list of metrics tracked by the application
- `GET /actuator/metrics/{name}` returns details of a specific metric by name
- `GET /actuator/beans` returns a list of beans in the ApplicationContext

# Future Improvements

- Request logging and metrics working with WebFlux
- Load test scenarios
- Resiliency features (retries, bulkhead pattern etc...)
- Caching where appropriate
- Connect metrics to Prometheus + Grafana dashboards
- Swagger docs (once supported with WebFlux)
- Mock API dependency for testing
