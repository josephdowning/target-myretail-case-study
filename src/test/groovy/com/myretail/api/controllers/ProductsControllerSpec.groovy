package com.myretail.api.controllers

import com.myretail.api.domain.api.Price
import com.myretail.api.domain.api.Product
import com.myretail.api.services.ProductService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import reactor.core.publisher.Mono
import spock.lang.Specification

class ProductsControllerSpec extends Specification {

    ProductService productService = Mock(ProductService)
    ProductsController controller = new ProductsController(productService: productService)

    def 'findById returns a 200 response if Product returned by ProductService'() {
        given:
        String productId = 'productId'
        Product product = new Product(id: productId, title: 'title', price: new Price(value: 9.99, currencyCode: 'USD'))

        when:
        ResponseEntity<Product> response = controller.findById(productId).block()

        then:
        1 * productService.getProduct(productId) >> Mono.just(product)
        0 * _

        response.statusCode == HttpStatus.OK
        response.body == product
    }

    def 'findById returns a 404 response if Product not returned by ProductService'() {
        given:
        String productId = 'productId'

        when:
        ResponseEntity<Product> response = controller.findById(productId).block()

        then:
        1 * productService.getProduct(productId) >> Mono.empty()
        0 * _

        response.statusCode == HttpStatus.NOT_FOUND
    }

    def 'setPrice returns a 201 response if ProductService has set the price'() {
        given:
        String productId = 'productId'
        Price price = new Price(value: 9.99, currencyCode: 'USD')
        Product product = new Product(id: productId, price: price)

        when:
        ResponseEntity<Product> response = controller.setPrice(productId, price).block()

        then:
        1 * productService.setPrice(productId, price) >> Mono.just(product)
        0 * _

        response.statusCode == HttpStatus.CREATED
        response.body == product
    }
}
