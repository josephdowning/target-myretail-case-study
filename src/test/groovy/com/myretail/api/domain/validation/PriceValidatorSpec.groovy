package com.myretail.api.domain.validation

import com.myretail.api.domain.api.Price
import org.springframework.validation.Errors
import spock.lang.Specification
import spock.lang.Unroll

class PriceValidatorSpec extends Specification {

    PriceValidator validator = new PriceValidator()

    def 'supports Price objects'() {
        expect:
        validator.supports(Price)
    }

    def 'does not support non-Price objects'() {
        expect:
        !validator.supports(String)
    }

    def 'accepts valid Price'() {
        given:
        Price price = new Price(value: 9.99, currencyCode: 'EUR')
        Errors mockErrors = Mock(Errors)

        when:
        validator.validate(price, mockErrors)

        then:
        0 * _
    }

    @Unroll
    def 'rejects Price #scenario'() {
        given:
        Price price = new Price(value: value, currencyCode: currencyCode)
        Errors mockErrors = Mock(Errors)

        when:
        validator.validate(price, mockErrors)

        then:
        1 * mockErrors.reject(errorCode, message)
        0 * _

        where:
        scenario                   | errorCode                | message                           | value | currencyCode
        'with no price'            | 'value.undefined'        | 'value must be specified'         | null  | 'USD'
        'with zero price'          | 'value.invalid'          | 'value must be greater than zero' | 0     | 'USD'
        'with negative price'      | 'value.invalid'          | 'value must be greater than zero' | -9.99 | 'USD'
        'with no currency code'    | 'currencyCode.undefined' | 'currencyCode must be specified'  | 9.99  | null
        'with empty currency code' | 'currencyCode.undefined' | 'currencyCode must be specified'  | 9.99  | ' '
    }
}
