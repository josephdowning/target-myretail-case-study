package com.myretail.api.services

import com.myretail.api.clients.ProductDataClient
import com.myretail.api.domain.dependencies.ProductDescription
import com.myretail.api.domain.dependencies.ProductItemData
import org.springframework.web.reactive.function.client.WebClientResponseException
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Unroll

class ProductDetailsServiceSpec extends Specification {

    ProductDataClient productDataClient = Mock()
    ProductDetailsService service = new ProductDetailsService(productDataClient: productDataClient)

    def 'Returns product data from remote product data service'() {
        given:
        String productId = 'productId'
        ProductItemData expected = new ProductItemData(tcin: productId, productDescription: new ProductDescription(title: 'title'))

        when:
        ProductItemData actual = service.getProductData(productId).block()

        then:
        1 * productDataClient.getProduct(productId) >> Mono.just(expected)
        0 * _

        actual == expected
    }

    @Unroll
    def 'Returns empty if call to remote product data service is unsuccessful'() {
        given:
        String productId = 'productId'

        WebClientResponseException e = new WebClientResponseException(
                'test error', statusCode, 'test error',
                null, null, null
        )

        when:
        ProductItemData actual = service.getProductData(productId).block()

        then:
        1 * productDataClient.getProduct(productId) >> Mono.error(e)
        0 * _

        actual == null

        where:
        statusCode << [400, 404, 500]
    }
}
