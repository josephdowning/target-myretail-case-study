package com.myretail.api.services

import com.myretail.api.domain.api.Price
import com.myretail.api.domain.db.PriceInfo
import com.myretail.api.domain.api.Product
import com.myretail.api.domain.dependencies.ProductDescription
import com.myretail.api.domain.dependencies.ProductItemData
import reactor.core.publisher.Mono
import spock.lang.Specification

class ProductServiceSpec extends Specification {

    ProductDetailsService productDataService = Mock()
    ProductPriceService productPriceService = Mock()

    ProductService service = new ProductService(
            productDataService: productDataService,
            productPriceService: productPriceService
    )

    def 'composes product and price info data'() {
        given:
        String productId = 'productId'
        ProductItemData productItemData = new ProductItemData(
                tcin: productId,
                productDescription: new ProductDescription(title: 'title')
        )
        PriceInfo priceInfo = new PriceInfo(value: 0.99, currencyCode: 'EUR')

        when:
        Product product = service.getProduct(productId).block()

        then:
        1 * productDataService.getProductData(productId) >> Mono.just(productItemData)
        1 * productPriceService.getPriceForProduct(productId) >> Mono.just(priceInfo)
        0 * _

        product.id == productId
        product.title == 'title'
        product.price.value == 0.99
        product.price.currencyCode == 'EUR'
        !product.price.error
    }

    def 'returns empty if no product data found'() {
        given:
        String productId = 'productId'
        PriceInfo priceInfo = new PriceInfo(value: 0.99, currencyCode: 'EUR')

        when:
        Product product = service.getProduct(productId).block()

        then:
        1 * productDataService.getProductData(productId) >> Mono.empty()
        1 * productPriceService.getPriceForProduct(productId) >> Mono.just(priceInfo)
        0 * _

        product == null
    }

    def 'returns product with degraded price info if no price info found'() {
        given:
        String productId = 'productId'
        ProductItemData productItemData = new ProductItemData(
                tcin: productId,
                productDescription: new ProductDescription(title: 'title')
        )

        when:
        Product product = service.getProduct(productId).block()

        then:
        1 * productDataService.getProductData(productId) >> Mono.just(productItemData)
        1 * productPriceService.getPriceForProduct(productId) >> Mono.empty()
        0 * _

        product.id == productId
        product.title == 'title'
        product.price.error == 'Price info unavailable'
    }

    def 'saves price info to productPriceService'() {
        given:
        String productId = 'productId'
        Price price = new Price(value: 0.99, currencyCode: 'EUR')
        PriceInfo priceInfo = new PriceInfo(value: 0.99, currencyCode: price.currencyCode)

        when:
        Product product = service.setPrice(productId, price).block()

        then:
        1 * productPriceService.setPriceInfoForProduct(productId, priceInfo) >> Mono.just(priceInfo)
        0 * _

        product.id == productId
        product.price.value == 0.99
        product.price.currencyCode == 'EUR'
    }
}
