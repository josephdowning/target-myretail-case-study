package com.myretail.api.services

import com.myretail.api.domain.db.PriceInfo
import com.myretail.api.repositories.PriceInfoRepository
import reactor.core.publisher.Mono
import spock.lang.Specification

class ProductPriceServiceSpec extends Specification {

    PriceInfoRepository priceInfoRepository = Mock(PriceInfoRepository)
    ProductPriceService productPriceService = new ProductPriceService(priceInfoRepository: priceInfoRepository)

    void 'getPriceForProduct delegates to repository'() {
        given:
        String productId = 'productId'
        PriceInfo expected = new PriceInfo(id: productId, value: 9.99, currencyCode: 'GBP')

        when:
        PriceInfo actual = productPriceService.getPriceForProduct(productId).block()

        then:
        1 * priceInfoRepository.findById(productId) >> Mono.just(expected)
        0 * _

        actual == expected
    }

    void 'setPriceForProduct delegates to repository'() {
        given:
        String productId = 'productId'
        PriceInfo expected = new PriceInfo(id: productId, value: 9.99, currencyCode: 'GBP')

        when:
        PriceInfo actual = productPriceService.setPriceInfoForProduct(productId, expected).block()

        then:
        1 * priceInfoRepository.save(expected) >> Mono.just(expected)
        0 * _

        actual == expected
    }
}
