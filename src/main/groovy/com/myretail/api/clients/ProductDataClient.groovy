package com.myretail.api.clients

import com.myretail.api.domain.dependencies.ProductDetailsResponse
import com.myretail.api.domain.dependencies.ProductItemData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.util.UriBuilder
import reactor.core.publisher.Mono

@Component
class ProductDataClient {
    private static final String EXCLUDE_FROM_REQUEST = ['taxonomy',
                                                        'price',
                                                        'promotion',
                                                        'bulk_ship',
                                                        'rating_and_review_reviews',
                                                        'rating_and_review_statistics',
                                                        'question_answer_statistics',
                                                        'deep_red_labels',
                                                        'available_to_promise_network'].join(',')

    private final WebClient client

    @Autowired
    ProductDataClient(WebClient.Builder builder) {
        client = builder
                .baseUrl('https://redsky.target.com')
                .build()
    }

    Mono<ProductItemData> getProduct(String productId) {
        return client.get()
                .uri(uriBuilder(productId))
                .retrieve()
                .bodyToMono(ProductDetailsResponse)
                .map { ProductDetailsResponse response -> response.product.item }
    }

    private Closure uriBuilder(String productId) {
        return { UriBuilder uriBuilder ->
            uriBuilder.path('v2/pdp/tcin/{productId}')
                    .queryParam('excludes', EXCLUDE_FROM_REQUEST)
                    .build(productId)
        }
    }
}
