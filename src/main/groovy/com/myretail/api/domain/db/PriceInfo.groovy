package com.myretail.api.domain.db

import com.myretail.api.annotations.DbObject
import org.springframework.data.annotation.Id

@DbObject
class PriceInfo implements Serializable {
    @Id
    String id
    BigDecimal value
    String currencyCode
}
