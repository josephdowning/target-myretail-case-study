package com.myretail.api.domain.dependencies

import com.fasterxml.jackson.annotation.JsonProperty
import com.myretail.api.annotations.ApiObject

@ApiObject
class ProductDetailsResponse {
    ProductDetails product
}

@ApiObject
class ProductDetails {
    ProductItemData item
}

@ApiObject
class ProductItemData {
    String tcin

    @JsonProperty('product_description')
    ProductDescription productDescription
}

@ApiObject
class ProductDescription {
    String title
}

