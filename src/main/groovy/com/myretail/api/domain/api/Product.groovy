package com.myretail.api.domain.api

import com.fasterxml.jackson.annotation.JsonProperty
import com.myretail.api.annotations.ApiObject

@ApiObject
class Product {
    String id
    String title

    @JsonProperty('current_price')
    Price price
}
