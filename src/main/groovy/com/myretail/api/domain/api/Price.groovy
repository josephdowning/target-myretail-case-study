package com.myretail.api.domain.api

import com.fasterxml.jackson.annotation.JsonProperty
import com.myretail.api.annotations.ApiObject

@ApiObject
class Price {
    BigDecimal value
    @JsonProperty('currency_code')
    String currencyCode
    String error
}
