package com.myretail.api.domain.validation

import com.myretail.api.domain.api.Price
import org.springframework.lang.Nullable
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import org.springframework.validation.Errors
import org.springframework.validation.Validator

@Component
class PriceValidator implements Validator {
    @Override
    boolean supports(Class<?> clazz) {
        return Price.isAssignableFrom(clazz)
    }

    @Override
    void validate(@Nullable Object target, Errors errors) {
        Price price = target as Price

        if (price.value == null) {
            errors.reject('value.undefined', 'value must be specified')
        } else if (price.value <= 0) {
            errors.reject('value.invalid', 'value must be greater than zero')
        }

        if (!StringUtils.hasText(price.currencyCode)) {
            errors.reject('currencyCode.undefined', 'currencyCode must be specified')
        }
    }
}
