package com.myretail.api

import com.myretail.api.domain.db.PriceInfo
import com.myretail.api.repositories.PriceInfoRepository
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component

@Slf4j
@Component
class ApplicationReadyTasks implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    PriceInfoRepository priceInfoRepository

    void bootstrapMongo() {
        createIfNeeded(new PriceInfo(id: '13860428', value: 9.99, currencyCode: 'USD'))
        createIfNeeded(new PriceInfo(id: '53255996', value: 19.99, currencyCode: 'USD'))
        createIfNeeded(new PriceInfo(id: '15117729', value: 29.99, currencyCode: 'USD'))
        createIfNeeded(new PriceInfo(id: '16483589', value: 14.99, currencyCode: 'USD'))
        createIfNeeded(new PriceInfo(id: '16696652', value: 12.99, currencyCode: 'USD'))
        createIfNeeded(new PriceInfo(id: '16752456', value: 7.49, currencyCode: 'USD'))
        createIfNeeded(new PriceInfo(id: '15643793', value: 3.00, currencyCode: 'USD'))
        log.info('Done bootstrapping Mongo')
    }

    @Override
    void onApplicationEvent(ApplicationReadyEvent event) {
        bootstrapMongo()
    }

    void createIfNeeded(PriceInfo priceInfo) {
        boolean exists = priceInfoRepository.existsById(priceInfo.id).block()
        if (!exists) {
            priceInfoRepository.insert(priceInfo).subscribe()
            log.info("Added $priceInfo to Mongo")
        }
    }
}
