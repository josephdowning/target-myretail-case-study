package com.myretail.api.repositories

import com.myretail.api.domain.db.PriceInfo
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository

@Repository
interface PriceInfoRepository extends ReactiveMongoRepository<PriceInfo, String> {
}
