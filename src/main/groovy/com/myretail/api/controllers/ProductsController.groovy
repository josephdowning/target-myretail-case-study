package com.myretail.api.controllers

import com.myretail.api.domain.api.Price
import com.myretail.api.domain.api.Product
import com.myretail.api.domain.validation.PriceValidator
import com.myretail.api.services.ProductService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

import javax.validation.Valid

@RequestMapping(value = '/myretail/v1/products')
@RestController
class ProductsController {

    @Autowired
    ProductService productService

    @Autowired
    PriceValidator priceValidator

    @GetMapping("/{productId}")
    Mono<ResponseEntity<Product>> findById(@PathVariable String productId) {
        return productService.getProduct(productId)
                .map(ResponseEntity.&ok)
                .switchIfEmpty(Mono.just(ResponseEntity.notFound().build()))
    }

    @PutMapping("/{productId}")
    Mono<ResponseEntity<Product>> setPrice(@PathVariable String productId, @Valid @RequestBody Price price) {
        return productService.setPrice(productId, price)
                .map { Product product -> new ResponseEntity<Product>(product, HttpStatus.CREATED) }
    }

    @InitBinder('price')
    void initPriceBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(priceValidator)
    }
}
