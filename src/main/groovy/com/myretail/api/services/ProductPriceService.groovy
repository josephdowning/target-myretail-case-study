package com.myretail.api.services

import com.myretail.api.domain.db.PriceInfo
import com.myretail.api.repositories.PriceInfoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class ProductPriceService {

    @Autowired
    PriceInfoRepository priceInfoRepository

    Mono<PriceInfo> getPriceForProduct(String productId) {
        return priceInfoRepository.findById(productId)
    }

    Mono<PriceInfo> setPriceInfoForProduct(String productId, PriceInfo priceInfo) {
        priceInfo.id = productId
        return priceInfoRepository.save(priceInfo)
    }
}
