package com.myretail.api.services

import com.myretail.api.domain.api.Price
import com.myretail.api.domain.db.PriceInfo
import com.myretail.api.domain.api.Product
import com.myretail.api.domain.dependencies.ProductItemData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class ProductService {

    @Autowired
    ProductDetailsService productDataService

    @Autowired
    ProductPriceService productPriceService

    Mono<Product> getProduct(String productId) {
        Mono<ProductItemData> itemDataMono = productDataService.getProductData(productId)
        Mono<Price> productPriceMono = productPriceService.getPriceForProduct(productId).map { PriceInfo priceInfo ->
            new Price(value: priceInfo.value, currencyCode: priceInfo.currencyCode)
        }.defaultIfEmpty(new Price(error: 'Price info unavailable'))

        return itemDataMono.zipWith(productPriceMono) { ProductItemData itemData, Price price ->
            new Product(id: productId, title: itemData.productDescription.title, price: price)
        }
    }

    Mono<Product> setPrice(String productId, Price price) {
        return productPriceService.setPriceInfoForProduct(
                productId,
                new PriceInfo(value: price.value, currencyCode: price.currencyCode)
        ).map { PriceInfo priceInfo ->
            Price finalPrice = new Price(value: priceInfo.value, currencyCode: priceInfo.currencyCode)
            return new Product(id: productId, price: finalPrice)
        }
    }
}
