package com.myretail.api.services

import com.myretail.api.clients.ProductDataClient
import com.myretail.api.domain.dependencies.ProductItemData
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClientResponseException
import reactor.core.publisher.Mono

@Slf4j
@Service
class ProductDetailsService {

    @Autowired
    ProductDataClient productDataClient

    Mono<ProductItemData> getProductData(String productId) {
        return productDataClient.getProduct(productId)
                .onErrorResume(WebClientResponseException) { WebClientResponseException e ->
                    log.warn("ProductData service responded with '${e.statusCode}' status for productId '$productId'")
                    log.debug('ProductData service error details:', e)
                    Mono.empty()
                }
    }
}
