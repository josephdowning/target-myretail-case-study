package com.myretail.api.annotations

import groovy.transform.AnnotationCollector
import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.springframework.data.mongodb.core.mapping.Document

@AnnotationCollector
@Document
@CompileStatic
@EqualsAndHashCode
@ToString(includePackage = false, ignoreNulls = false, includeNames = true)
@interface DbObject { }
