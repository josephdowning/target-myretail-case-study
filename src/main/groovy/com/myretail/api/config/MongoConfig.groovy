package com.myretail.api.config

import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoClients
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories

@EnableMongoRepositories(basePackages = 'com.myretail.api.repositories')
@EnableReactiveMongoRepositories(basePackages = 'com.myretail.api.repositories')
@Configuration
class MongoConfig extends AbstractReactiveMongoConfiguration {

    @Override
    MongoClient reactiveMongoClient() {
        return MongoClients.create()
    }

    @Override
    protected String getDatabaseName() {
        return 'myretail'
    }
}
