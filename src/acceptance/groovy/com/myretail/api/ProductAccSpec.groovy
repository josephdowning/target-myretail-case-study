package com.myretail.api

import com.myretail.api.client.ApiTestClient
import com.myretail.api.client.ClientFactory
import okhttp3.HttpUrl
import retrofit2.Response
import spock.lang.Specification
import spock.lang.Unroll

import java.security.SecureRandom

class ProductAccSpec extends Specification {

    ApiTestClient client = new ClientFactory(
            new HttpUrl.Builder()
                    .scheme('http')
                    .host('localhost')
                    .port(8080)
                    .addPathSegments('myretail/v1/')
                    .build()
    ).build()

    SecureRandom secureRandom = new SecureRandom()

    @Unroll
    def 'getProduct returns product data'() {
        when:
        Response<Map> response = client.getProduct(productId).execute()

        then:
        response.code() == 200
        response.body().title == title
        response.body().current_price.value == price
        response.body().current_price.currency_code == 'USD'

        where:
        productId  | title                                             | price
        '13860428' | 'The Big Lebowski (Blu-ray)'                      | 9.99
        '53255996' | 'Band-Aid Brand Build Your Own First Aid Kit Bag' | 19.99
    }

    def 'getProduct returns 404 status if no product details are found'() {
        given:
        String productId = 'not-in-red-sky'

        when:
        Response<Map> response = client.getProduct(productId).execute()

        then:
        response.code() == 404
    }

    def 'getProduct returns degraded response if no price info is found for product'() {
        given:
        String productId = '53389268'

        when:
        Response<Map> response = client.getProduct(productId).execute()

        then:
        response.code() == 200
        response.body().title == 'Black Panther (Blu-ray + Digital)'
        response.body().current_price.error == 'Price info unavailable'
    }

    def 'setPrice saves price info for the specified product'() {
        given:
        String productId = '53329100'
        BigDecimal newPrice = secureRandom.nextInt(100) + secureRandom.nextInt(100) / 100

        Map priceInfo = [
                value        : newPrice,
                currency_code: 'USD'
        ]

        when:
        Response<Map> setResponse = client.setPrice(productId, priceInfo).execute()

        then:
        setResponse.code() == 201
        setResponse.body().current_price.value == newPrice
        setResponse.body().current_price.currency_code == 'USD'

        when:
        Response<Map> getResponse = client.getProduct(productId).execute()

        then:
        getResponse.code() == 200
        getResponse.body().current_price.value == newPrice
        getResponse.body().current_price.currency_code == 'USD'
    }

    @Unroll
    def 'setPrice returns 400 if #scenario'() {
        given:
        String productId = '53329100'

        Map priceInfo = [
                value        : newPrice,
                currency_code: currencyCode
        ]

        when:
        Response<Map> setResponse = client.setPrice(productId, priceInfo).execute()

        then:
        setResponse.code() == 400

        where:
        scenario                        | newPrice | currencyCode
        'no price is specified'         | null     | 'USD'
        'price is negative'             | -9.99    | 'USD'
        'price is zero'                 | 0        | 'USD'
        'no currency code is specified' | 9.99     | null
        'currency code is blank'        | 9.99     | ' '
    }
}
