package com.myretail.api

import com.myretail.api.client.ApiTestClient
import com.myretail.api.client.ClientFactory
import okhttp3.HttpUrl
import retrofit2.Response
import spock.lang.Specification

class HealthAccSpec extends Specification {

    ApiTestClient client = new ClientFactory(
            new HttpUrl.Builder()
                    .scheme('http')
                    .host('localhost')
                    .port(8080)
                    .build()
    ).build()

    def 'App returns healthy'() {
        when:
        Response<Map> response = client.health().execute()

        then:
        response.code() == 200
        response.body().status == 'UP'
    }
}
