package com.myretail.api.client

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

interface ApiTestClient {

    @GET('actuator/health')
    Call<Map> health()

    @GET('products/{productId}')
    Call<Map> getProduct(@Path('productId') String productId)

    @PUT('products/{productId}')
    Call<Map> setPrice(@Path('productId') String productId, @Body Map price)
}
