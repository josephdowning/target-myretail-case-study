package com.myretail.api.client

import okhttp3.HttpUrl
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

class ClientFactory {

    private final Retrofit retrofit

    ClientFactory(HttpUrl httpUrl) {
        retrofit = new Retrofit.Builder()
                .baseUrl(httpUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
    }

    ApiTestClient build() {
        return retrofit.create(ApiTestClient)
    }
}
